extends KinematicBody

#onready var anim_tree = get_node("AnimationTree")
var direction = Vector3.BACK
var velocity = Vector3.ZERO
var strafe_dir = Vector3.ZERO
var strafe = Vector3.ZERO
var aim_turn = 0
var bozowin = 3

var emahp = 3

var vertical_velocity = 0
var gravity = 20

var movement_speed = 0
var walk_speed = 10
var run_speed = 15
var acceleration = 5
var angular_acceleration = 9

var roll_magnitude = 17

func _ready():
	direction = Vector3.BACK.rotated(Vector3.UP, $Camroot/h.global_transform.basis.get_euler().y)
	# Sometimes in the level design you might need to rotate the Player object itself
	# So changing the direction at the beginning


func _input(event):
	if event is InputEventMouseMotion:
		aim_turn = -event.relative.x * 0.015 #animates player with mouse movement while aiming (used in line 104)



func _physics_process(delta):
#	print(Performance.get_monitor(Performance.TIME_FPS)) # Prints the FPS to the console

	var h_rot = $Camroot/h.global_transform.basis.get_euler().y
	
	if Input.is_action_pressed("forward") ||  Input.is_action_pressed("backward") ||  Input.is_action_pressed("left") ||  Input.is_action_pressed("right"):
	
		direction = Vector3(Input.get_action_strength("left") - Input.get_action_strength("right"),0,Input.get_action_strength("forward") - Input.get_action_strength("backward"))
	
		strafe_dir = direction
	
		direction = direction.rotated(Vector3.UP, h_rot).normalized()
	
		movement_speed = walk_speed
	
	#			$AnimationTree.set("parameters/iwr_blend/blend_amount", lerp($AnimationTree.get("parameters/iwr_blend/blend_amount"), 0, delta * acceleration))
	else:
	#		$AnimationTree.set("parameters/iwr_blend/blend_amount", lerp($AnimationTree.get("parameters/iwr_blend/blend_amount"), -1, delta * acceleration))
		movement_speed = 0
		direction = $Camroot/h.global_transform.basis.z
	
	velocity = lerp(velocity, direction * movement_speed, delta * acceleration)
	#		if sqrt(velocity.x*velocity.x + velocity.y*velocity.y) > 0.001:
	move_and_slide(velocity + Vector3.DOWN * vertical_velocity, Vector3.UP)
	
	$Mesh.rotation.y = lerp_angle($Mesh.rotation.y, $Camroot/h.rotation.y, delta * angular_acceleration)
	# lerping towards $Camroot/h.rotation.y while aiming, h_rot(as in the video) doesn't work if you rotate Player object
	strafe = lerp(strafe, strafe_dir + Vector3.RIGHT * aim_turn, delta * acceleration)
	
	if !is_on_floor():
		vertical_velocity += gravity * delta
	else:
		vertical_velocity = 0
