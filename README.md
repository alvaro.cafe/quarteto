# Quarteto

Use WASD e o mouse para se mover pelos instrumentos. A música é "Little" Fugue in G Minor do Bach arranjada para quarteto de cordas.

Modelo do violino: https://opengameart.org/content/violin

Música: https://musescore.org/en/node/112186

## Screenshot


![Ande pelos instrumentos.](./thumb.png)